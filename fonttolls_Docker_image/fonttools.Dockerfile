# docker build -f ./fonttools.Dockerfile -t dosmode/fonttools:0.1.2 -t dosmode/fonttools:latest ./

FROM debian:buster-slim as run

# Set one or more individual labels
LABEL dosmode.fonttools.version="4.8.1"
LABEL dosmode.fonttools.release-date="2020-04-22"
LABEL URL="https://dosmo.de/posts/tools-and-docker-images"
LABEL GitURL="https://gitlab.com/dosmode/tools.git"

# Read this:
# https://developers.google.com/web/fundamentals/performance/optimizing-content-efficiency/webfont-optimization
# and this:
# https://docs.microsoft.com/en-us/typography/opentype/spec/name#name-ids

# Since v0.1.3 from 2019-09-27 with fonttools ≥ 4.0.2
# Since 4.6.0 tracking the release version of fonttools

SHELL ["/bin/bash", "-c"]

RUN apt-get update && \
apt-get -y install \
                    apt-transport-https \
                    busybox             \
                    ca-certificates     \
                    curl                \
                    dirmngr             \
                    git                 \
                    libncursesw5        \
                    libtinfo5           \
                    lsb-release         \
                    python3-pip         \
                    vim-tiny            \
                    woff2            && \
mkdir -vp /usr/local/bin/busybox && \
busybox --install /usr/local/bin/busybox && PATH=$PATH:/usr/local/bin/busybox && \
curl -JL https://gitlab.com/dosmode/tools/raw/master/skel_bashrc -o $HOME/.bashrc && \
curl -JL https://gitlab.com/dosmode/tools/raw/master/bash_aliases -o $HOME/.bash_aliases && \
hstr_url=$(curl "https://api.github.com/repos/dvorka/hstr/releases/latest" | \
    awk -F'"' '/https:.+hstr_.+_amd64.deb/{print $(NF-1)}' ) && \
curl -JL $hstr_url -o /tmp/hstr.deb && \
/usr/bin/dpkg -i /tmp/hstr.deb && rm -v /tmp/hstr.deb && . $HOME/.bashrc && \
pip3 install fonttools[ufo,lxml,woff,unicode] zopfli brotli

WORKDIR /opt

# --text-file=../chars_medium.txt --layout-features+=onum,pnum,liga --verbose
# --flavor=woff --with-zopfli
# --flavor=woff2

# for f in *.woff ; do pyftsubset $f --text-file=../chars_medium.txt --layout-features+=onum,pnum,liga --name-IDs+=0,4,6,8,9,10 --verbose --flavor=woff --with-zopfli --output-file=subset_medium/$f ; done 

# for f in *.woff ; do pyftsubset $f --text-file=../chars_medium.txt --layout-features+=onum,pnum,liga --name-IDs+=0,4,6,8,9,10 --verbose --flavor=woff2 --output-file=subset_medium/${f}2 ; done