# docker build -f ./lamp.Dockerfile -t dosmode/lamp:0.1.0 -t dosmode/lamp:latest ./

FROM debian:buster-slim as run

# Set one or more individual labels
LABEL dosmode.lamp.version="0.1.0"
LABEL dosmode.lamp.release-date="2019-09-30"
LABEL URL="https://dosmo.de/posts/tools-and-docker-images"
LABEL GitURL="https://gitlab.com/dosmode/tools.git"

SHELL ["/bin/bash", "-c"]

RUN echo "deb http://deb.debian.org/debian buster-backports main"  >> /etc/apt/sources.list && \
apt update && apt upgrade -y && \
apt install -y \
    apache2/buster-backports \
    apt-transport-https      \
    busybox                  \
    ca-certificates          \
    composer                 \
    curl                     \
    libapache2-mod-php       \
    libncursesw5             \
    libtinfo5                \
    mariadb-client           \
    mariadb-server           \
    php                      \
    php-bcmath               \
    php-cli                  \
    php-curl                 \
    php-fpm                  \
    php-gd                   \
    php-json                 \
    php-mbstring             \
    php-mysql                \
    php-pdo                  \
    php-pear                 \
    php-xml                  \
    php-zip                  \
    vim-tiny                 && \

cd /var/www/html/ && \
if [ ! -d  /var/www/html/phpmyadmin/ ]; then \
  composer create-project phpmyadmin/phpmyadmin --repository-url=https://www.phpmyadmin.net/packages.json --no-dev ; \
fi && \

mkdir -vp /usr/local/bin/busybox && \
busybox --install /usr/local/bin/busybox && PATH=$PATH:/usr/local/bin/busybox && \
curl -JL https://gitlab.com/dosmode/tools/raw/master/skel_bashrc -o $HOME/.bashrc && \
curl -JL https://gitlab.com/dosmode/tools/raw/master/bash_aliases -o $HOME/.bash_aliases && \
hstr_url=$(curl "https://api.github.com/repos/dvorka/hstr/releases/latest" | \
    awk -F'"' '/https:.+hstr_.+_amd64.deb/{print $(NF-1)}' ) && \
curl -JL $hstr_url -o /tmp/hstr.deb && \
/usr/bin/dpkg -i /tmp/hstr.deb && rm -v /tmp/hstr.deb && . $HOME/.bashrc 

EXPOSE 80

# docker run -it -h lamp --name lamp -v lamp -p 8880:80 dosmode/lamp
