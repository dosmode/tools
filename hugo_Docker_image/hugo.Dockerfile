# docker build -f ./hugo.Dockerfile -t dosmode/hugo:0.1.6 -t dosmode/hugo:latest ./

FROM debian:buster-slim as run

# Set one or more individual labels
LABEL dosmode.hugo.version="0.68.3"
LABEL dosmode.hugo.release-date="2020-03-24"
LABEL URL="https://dosmo.de/posts/tools-and-docker-images"
LABEL GitURL="https://gitlab.com/dosmode/tools.git"

SHELL ["/bin/bash", "-c"]

RUN apt-get update && \
apt-get -y install \
                    apt-transport-https \
                    busybox             \
                    ca-certificates     \
                    curl                \
                    dirmngr             \
                    git                 \
                    libncursesw5        \
                    libtinfo5           \
                    lsb-release      && \
mkdir -vp /usr/local/bin/busybox && \
busybox --install /usr/local/bin/busybox && PATH=$PATH:/usr/local/bin/busybox && \
curl -JL https://gitlab.com/dosmode/tools/raw/master/skel_bashrc -o $HOME/.bashrc && \
curl -JL https://gitlab.com/dosmode/tools/raw/master/bash_aliases -o $HOME/.bash_aliases && \
hstr_url=$(curl "https://api.github.com/repos/dvorka/hstr/releases/latest" | \
    awk -F'"' '/https:.+hstr_.+_amd64.deb/{print $(NF-1)}' ) && \
curl -JL $hstr_url -o /tmp/hstr.deb && \
/usr/bin/dpkg -i /tmp/hstr.deb && rm -v /tmp/hstr.deb && . $HOME/.bashrc && \
## install node 10.x LTS via the recommended way O_o 
# https://github.com/nodesource/distributions/blob/master/README.md#installation-instructions
curl -sL https://deb.nodesource.com/setup_12.x | bash - && \
apt-get -y install nodejs && \
# update npm to the latest version
npm i -g npm@latest &&\
# install global packages
npm i -g sass postcss-cli autoprefixer && \
# get latest hugo binary from github, unpack to /usr/local/bin , which is in $PATH
dl_url=$(curl "https://api.github.com/repos/gohugoio/hugo/releases/latest" | \
    awk -F'"' '/https:.+hugo_extended_.+Linux-64bit.tar.gz/{print $(NF-1)}' ) && \
curl -JL $dl_url | tar xzv -C /usr/local/bin 

# # add yarn: https://yarnpkg.com/en/docs/install#debian-stable
# curl -sS https://dl.yarnpkg.com/debian/pubkey.gpg |  apt-key add - && \
# echo "deb https://dl.yarnpkg.com/debian/ stable main" |  tee /etc/apt/sources.list.d/yarn.list && \
# apt-get update && apt-get install yarn && \

EXPOSE 1313

WORKDIR /opt

CMD /usr/local/bin/hugo server \
    --bind 0.0.0.0 \
    --navigateToChanged \
    --templateMetrics \
    --buildDrafts

# build site by opening terminal in container with:
# docker exec -it hugo bash
# in container run:
# hugo -v --cleanDestinationDir --gc --minify 
# this is aliased in bash_aliases as hubu