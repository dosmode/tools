#!/usr/bin/env bash

if ls --color &> /dev/null; then 
  alias ll='ls -kahlF --color=always'
  alias l='ls -ACF --color=always'
else 
  alias ll='ls -kahlFG'
  alias l='ls -ACFG'
fi

alias cd..='cd ..'
alias mkdir='mkdir -pv'
if git --version &> /dev/null; then
  alias git-root='cd "$(git rev-parse --show-toplevel)"'
fi

if hugo version &> /dev/null; then
  alias hubu='hugo -v --cleanDestinationDir --gc --minify'
fi

alias bashrc="vi ~/.bashrc && source ~/.bashrc"